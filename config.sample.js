module.exports = {
    // location
    protocol: 'http',
    url: 'localhost',
    port: 3000,

    // full url
    fullUrl: this.protocol + '://' + this.url + (String(this.port).length > 0 ? ':' + this.port : ''),

    // database credentials
    db: {
        port: 27017,
        host: 'localhost',
        name: 'weather',
        user: 'username',
        password: 'password',
    },

    redis: {
        host: '127.0.0.1',
        port: 6379,
        ttl: 86400,
        secret: 'somethingSpecialLikeRandomString',
    },
};