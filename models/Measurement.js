/**
 * Measurement database model
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */
const moment = require('moment');
moment.locale('cs');
// library for easy database manipulations
const mongoose = require('../libs/db');

// the schema itself
var measurementSchema = new mongoose.Schema({
    author: {
        type: String,
    },
    location: {
        type: String,
    },
    temperature: {
        type: Number,
    },
    humidity: {
        type: Number,
    },
    altitude: {
        type: Number,
    },
    soundVolume: {
        type: Number,
    },
    light: {
        type: Number,
    },
    date: {
        type: Date,
        default: moment(),
    },
});

// export
module.exports = mongoose.model('Measurement', measurementSchema, 'measurement');