/**
 * Page controller
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */

/**
 * Models
 */

module.exports.loginPage = (req, res) => {
    res.render('login/login', { req, res });
};

module.exports.newMeasurement = (req, res) => {
    res.render('measurements/new', {req, res});
};

module.exports.homepage = (req, res) => {
    res.render('homepage', {req, res});
};

/*
module.exports.forgetPasswordPage = (req, res) => {
    res.render('login/forgot-password', { req, res });
};

module.exports.registerPage = (req, res) => {
    res.render('login/register', { req, res });
};*/