/**
 * Measurement controller
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */
const moment = require('moment');

/**
 * Models
 */
const Measurement = require('../models/Measurement');

module.exports.getAll = (req, res) => {
    Measurement.find({}, (err, measurements) => {
        if (err) {
            return console.error(err);
        }
        return res.send(measurements);
    });
};


module.exports.getByAuthor = (req, res) => {
    Measurement.find({
        author: req.params.author,
    }, (err, measurements) => {
        if (err) {
            return console.error(err);
        }
        return res.send(measurements);
    });
};

module.exports.new = (req, res) => {
    //console.log(req.body);
    //console.log(req.query);
    new Measurement({
        author: (typeof req.body.author != 'undefined' ? req.body.author : (typeof req.query.author != 'undefined' ? req.query.author : 'Anonym')),
        location: (typeof req.body.location != 'undefined' ? req.body.location : (typeof req.query.location != 'undefined' ? req.query.location : null)),
        temperature: (typeof req.body.temperature != 'undefined' ? req.body.temperature : (typeof req.query.temperature != 'undefined' ? req.query.temperature : null)),
        humidity: (typeof req.body.humidity != 'undefined' ? req.body.humidity : (typeof req.query.humidity != 'undefined' ? req.query.humidity : null)),
        altitude: (typeof req.body.altitude != 'undefined' ? req.body.altitude : (typeof req.query.altitude != 'undefined' ? req.query.altitude : null)),
        soundVolume: (typeof req.body.soundVolume != 'undefined' ? req.body.soundVolume : (typeof req.query.soundVolume != 'undefined' ? req.query.soundVolume : null)),
        light: (typeof req.body.light != 'undefined' ? req.body.light : (typeof req.query.light != 'undefined' ? req.query.light : null)),
        date: moment(),
    }).save((err, measurement) => {
        if (err) {
            return console.error(err);
        }
        console.log(`New measurement:\n${measurement}`);
        return res.send('ok');
    });
};