/**
 * The entry router of the app
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 * @see https://lukasmatuska.cz/
 */

/**
 * Express router API
 */
const router = require('express').Router();

/**
 * Controllers
 */
const pageController = require('../controllers/page');
const errorController = require('../controllers/error');
const measurementController = require('../controllers/measurement');

router.get('/', (req, res) => {
    pageController.homepage(req, res);
});

router.get('/new', (req, res) => {
    pageController.newMeasurement(req, res);
});


router.get('/login', (req, res) => {
    pageController.loginPage(req, res);
});

router.get('*', (req, res) => {
    errorController.error404(req, res);
});

module.exports = router;