/**
 * The entry router of the app
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 * @see https://lukasmatuska.cz/
 */

/**
 * Express router API
 */
const router = require('express').Router();

/**
 * Controllers
 */
const pageController = require('../controllers/page');
const errorController = require('../controllers/error');
const measurementController = require('../controllers/measurement');

router.get('/', (req, res) => {
    res.redirect('/api/getAll');
});

router.get('/get/:author', (req, res) => {
    measurementController.getByAuthor(req, res);
});

router.get('/getAll', (req, res) => {
    measurementController.getAll(req, res);
});

router.post('/new', (req, res) => {
    measurementController.new(req, res);
});

router.get('/new', (req, res) => {
    measurementController.new(req, res);
});

router.get('*', (req, res) => {
    res.send('404');
});

module.exports = router;